<?php
    // Template Name: Form
?>

<?php get_header(); ?>

<section>
    <h2>Contato</h2>
    <form action="" method="post">
        <div>
            <label for="nome">Nome:</label>
            <input type="text" id="nome" />
        </div>
        <div>
            <label for="email"><br/>E-mail:</label>
            <input type="email" id="email" />
        </div>
        <div>
            <label for="msg"><br/>Mensagem:<br/></label>
            <textarea id="msg" cols="50" rows="10"></textarea>
        </div>
        <div>
            <input class="submit" type="submit" value="Enviar">
        </div>
    </form>
</section>

<?php get_footer(); ?>