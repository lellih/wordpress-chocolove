<?php
    // Template Name: Home Page
?>

<?php get_header(); ?>

<main class="intro">
    <h1><?php the_field('titulo-slogan') ?></h1>
    <img class="caller" src="<?php echo get_stylesheet_directory_uri(); ?>/images/pexels-pixabay-menor.jpg" width="800" height="500"/>
</main>

<!--
<section class="sobre">

    <h2>Uma Mistura de</h2>
    <div class="container">
        <div class="sobre-item grid-6">
            <img src="img/cafe-1.jpg">
            <h3>amor</h3>
        </div>
        <div class="sobre-item grid-6">
            <img src="img/cafe-2.jpg">
            <h3>perfeição</h3>
        </div>
    </div>
</section>
-->

<!--
<section class="locais">
    <div class="locais-item container">
        <div class="grid-6">
            <img src="img/botafogo.jpg" alt="Brafé unidade Botafogo">
        </div>
        <div class="grid-6">
            <h2>Botafogo</h2>
            <p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo.</p>
            <a href="#">Ver Mapa</a>
        </div>
    </div>
    
    <div class="locais-item container">
        <div class="grid-6">
            <img src="img/iguatemi.jpg" alt="Brafé unidade Iguatemi">
        </div>
        <div class="grid-6">
            <h2>Iguatemi</h2>
            <p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo.</p>
            <a href="#">Ver Mapa</a>
        </div>
    </div>
    
    <div class="locais-item container">
        <div class="grid-6">
            <img src="img/botafogo.jpg" alt="Brafé unidade Mineirão">
        </div>
        <div class="grid-6">
            <h2>Mineirão</h2>
            <p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo.</p>
            <a href="#">Ver Mapa</a>
        </div>
    </div>

</section>


<section class="assine">
    <div class="container">
        <div class="assine-info grid-6">
            <h2>Assine Nossa Newsletter</h2>
            <p>promoções e eventos mensais</p>
        </div>
        <form class="grid-6">
            <label>E-mail</label>
            <input type="text" placeholder="Digite seu e-mail">
            <button type="submit">Enviar</button>
        </form>
    </div>
</section>
-->

<?php get_footer(); ?>