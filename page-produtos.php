<?php
    // Template Name: Produtos
?>

<?php get_header(); ?>

<section class="produtos">
    <h2><?php the_field('titulo') ?></h2>
    <div class="container">
        <div class="left">
            <div>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/choc-croc.jpg" alt="Chocolate Crocante">
            </div>
            <h3><?php the_field('subtitulo_1') ?></h3>
            <p><?php the_field('descricao_1') ?></p>
        </div>
        <div class="right">
            <div>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/choc-aoleite.jpg" alt="Chocolate Ao Leite">
            </div>
            <h3><?php the_field('subtitulo_2') ?></h3>
            <p><?php the_field('descricao_2') ?></p>
        </div>
        <div class="left">
            <div>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/choc-branco.jpg" alt="Chocolate Branco">
            </div>
            <h3><?php the_field('subtitulo_3') ?></h3>
            <p><?php the_field('descricao_3') ?></p>
        </div>
        <div class="right">
            <div>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pexels-freestocksorg-edit.jpg" alt="Chocolate Amargo">
            </div>
            <h3><?php the_field('subtitulo_4') ?></h3>
            <p><?php the_field('descricao_4') ?></p>
        </div>
</section>

<?php get_footer(); ?>