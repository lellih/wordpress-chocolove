<?php
    // Template Name: Header
?>

<!DOCTYPE html>
<html>
<head>
    <title><?php 
        if(is_front_page()){
            echo 'Chocolove';
        }elseif(is_page()){
            echo wp_title();
        }
        elseif(is_search()){
            echo 'Busca -';
        }elseif(!(is_404())&&(is_single())||(is_page())){
            echo wp_title();
            echo '-';
        }elseif(is_404()){
            echo 'Não encontrada';
    }?></title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/reset.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css">
    <?php wp_head() ?>
</head>
<body>
	<header>
        <nav class="container">
            <a class="header" href="<?php if(is_front_page()){echo "#";}else{echo get_home_url();} ?>">Chocolove</a>	
            <?php
            $args = array(
                'menu' => 'principal',
                'container' => false
            );
            wp_nav_menu($args)?>
            <!--
            <ul>
                <li><a href="/sobre">Sobre</a></li>
                <li><a href="/produtos">Produtos</a></li>
                <li><a href="#">Notícias</a></li>
                <li><a href="/contato">Contato</a></li>
            </ul>
            -->
        </nav>
    </header>