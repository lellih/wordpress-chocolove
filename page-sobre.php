<?php
    // Template Name: Sobre
?>

<?php get_header(); ?>

<section>
    <h2><?php the_field('titulo') ?></h2>
    <div class="sobre">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/sobre.jpg" />
        <p><?php the_field('texto') ?></p>
    </div>
    <div class="social-icons">
        <a href="<?php the_field('rede_social_2') ?>">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/instagram.png" />
        </a>
        <a href="<?php the_field('rede_social_1') ?>">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/facebook.png" />
        </a>
    </div>
</section>

<?php get_footer(); ?>